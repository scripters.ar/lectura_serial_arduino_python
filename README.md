# Serial_Read_Python_Arduino

## Introduction

If you have problems communicating your Arduino device with your PC, this project will help you, this file is responsible for reading the serial port of your PC and writing the data in an easily accessible .csv file.

### Prerequisites

This library is only for version python >= 3.4

## Installation

Installing

```
pip install pyserial
```

Usage

1) Indicate the port where your arduino is connected, modify the value of the port variable :  

```
In Windows COM1, COM2, COMn ...
  port = 'COM5'
```
```
In Linux 
  port = 'TTYUSB'
```

2) Indicate the baud rate, default will be 9600

```
baudRate = 9600
```

3)Verify the data in file:  

```
dataArduino.csv
```

## References

* [PySerial](https://pythonhosted.org/pyserial/pyserial.html#installation) - PySerial Documentation

## Authors

* **Edermar Dominguez** - [Ederdoski](https://gitlab.com/Ederdoski/about)
* **Anniel Luna** - [Anniel](https://gitlab.com/hoy.anniel)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)
