import serial
import time

port 	 = 'COM5'
baudRate = 9600

ser = serial.Serial(port, baudRate, timeout=0)

while 1:

	file    = open("arduinoData.csv", "w")
	actLine = str(ser.readline())

	newstr  = actLine.replace("b\'", "")
	newstr  = newstr.replace("b''", "")
	newstr  = newstr.replace("\\r", "")
	newstr  = newstr.replace("\\n", "")
	newstr  = newstr.replace("'", "")

	if newstr == "":
		newstr = -1

	if newstr != -1:
		#print(newstr)
		file.write(newstr)
		file.close()
		time.sleep(0.01);